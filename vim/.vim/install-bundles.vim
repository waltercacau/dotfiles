set nocompatible              " be iMproved, required

" Host specific
let s:hostvimrc = expand($HOME . '/.host.vimrc')
if filereadable(s:hostvimrc)
  exec ':so ' . s:hostvimrc
endif

" Loading Vundle
source ~/.vim/bundles.vim
