set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'fatih/vim-go'
if exists("enable_fzf") && enable_fzf
  Plugin 'junegunn/fzf.vim'
else
  Plugin 'kien/ctrlp.vim'
endif
Plugin 'vim-scripts/StatusLineHighlight'
if !exists("disable_ycm_from_vundle") || !disable_ycm_from_vundle
  Plugin 'ycm-core/YouCompleteMe'
endif
Plugin 'sickill/vim-monokai'
Plugin 'wting/rust.vim'
Plugin 'ConradIrwin/vim-bracketed-paste'
if !exists("disable_gitgutter") || !disable_gitgutter
  Plugin 'airblade/vim-gitgutter'
endif
Plugin 'nathangrigg/vim-beancount'

call vundle#end()
