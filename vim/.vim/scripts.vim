if did_filetype()
    finish
endif
if getline(1) =~# '^#!.*/bin/sh$'
    setfiletype sh
endif
