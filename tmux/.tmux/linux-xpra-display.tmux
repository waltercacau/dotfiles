# This will force all tmux created sessions to talk to
# the an xpra display at :1008.
#
# This allows both programs running inside tmux to communicate
# with X11 (e.g. vim can output to the X11 clipboard). Also,
# if you have xclip installed, the tmux-yank plugin will allow
# you to copy to the clipboard from tmux.
#
# It assumes you have some way to have xpra started
# using: xpra start :1008
#
# Sample install:
#   apt-get install xclip # so tmux can output to X11 clipboard.
#   echo source-file ~/.tmux/linux-xpra-display.tmux >> ~/.host.tmux
set -g update-environment ""
set-env -g DISPLAY :1008
