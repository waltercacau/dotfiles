# This will reattach every new window to the user namespace,
# which allows vim and tmux to interact with the clipboard.
#
# Sample install:
#   brew install reattach-to-user-namespace
#   echo source-file ~/.tmux/mac-reattach-user.tmux >> ~/.host.tmux
set-option -g default-command "reattach-to-user-namespace -l bash"
