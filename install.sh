#!/bin/bash
set -e
ROOT="$(cd "`dirname $0`"; pwd)";
cd $ROOT
setup_link() {
  ORIG="$ROOT/$1"
  DEST="$HOME/$(basename $1)"
  if [ -L "$DEST" ]; then
    rm -rf "$DEST"
  elif [ -e "$DEST" ]; then
    rm -ri "$DEST"
  fi
  if [ -e "$ORIG" ]; then
    echo "Creating link" "$DEST"
    ln -s "$ORIG" "$DEST"
  fi
}
setup_link vim/.vim
setup_link vim/.vimrc # forces deletion
setup_link tmux/.tmux.conf
setup_link tmux/.tmux
setup_link xpra/.xpra

create_empty_if_missing() {
  if [ ! -e "$1" ]; then
    mkdir -p "$(dirname "$1")"
    touch "$1"
  fi
}
create_empty_if_missing ~/.host.vimrc
create_empty_if_missing ~/.host.tmux

git submodule update --init
vim -u ~/.vim/install-bundles.vim +PluginInstall +qall
tmux new -s bootstrap-tmux $HOME/.tmux/plugins/tpm/scripts/install_plugins.sh

